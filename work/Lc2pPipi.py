import tensorflow as tf

from timeit import default_timer as timer

import sys, os
sys.path.append("../lib/")
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT
from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *

mlc = 2.286
mp  = 0.938
mpim  = 0.140
mpip = 0.140

db = Const(5.)
dr = Const(1.5)

cache = True

def OrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

def CouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

resonances = {
#  "L1115" : {
#    "channel"   : 2,
#    "mass"      : Const(1.115),
#    "width"     : Const(0.00001),
#    "spin"      : 1,
#    "parity"    : 1,
#    "couplings" : [
#        Complex(Const(1.), Const(0.) ),
#        Complex(Const(0.), Const(0.) )
#                  ]
#  },
  "L1670" : {
    "channel"   : 2,
    "mass"      : Const(1.661),
    "width"     : Const(0.08),
    "spin"      : 1,
    "parity"    : -1,
    "couplings" : [
	Complex(Const(1.), Const(0.) ),
	Complex(Const(0.), Const(0.) ),
                  ]
  },
  "S1385" : {
     "channel"    : 2,
     "mass"       : Const(1.385),
     "width"      : Const(0.1),
     "spin"       : 3,
     "parity"     : 1,
     "couplings"  : [
        Complex(FitParameter("ArS1385_1",  1., -20., 20., 0.01), FitParameter("AiS1385_1",  0.0, -20., 20., 0.01) ),
        Complex(FitParameter("ArS1385_2",  0., -20., 20., 0.01), FitParameter("AiS1385_2",  0.0, -20., 20., 0.01) )
                    ]
  },
#  "S1660" : {
#     "channel"    : 2,
#     "mass"       : Const(1.660),
#     "width"      : Const(0.075),
#     "spin"       : 1,
#     "parity"     : 1,
#     "couplings"  : [
#        Complex(FitParameter("ArS1660_1",  1., -20., 20., 0.01), FitParameter("AiS1660_1",  0.0, -20., 20., 0.01) ),
#        Complex(FitParameter("ArS1660_2",  0., -20., 20., 0.01), FitParameter("AiS1660_2",  0.0, -20., 20., 0.01) )
#                    ]
#  },
#  "S1730" : {
#     "channel"    : 2,
#     "mass"       : Const(1.730),
#     "width"      : Const(0.075),
#     "spin"       : 3,
#     "parity"     : 1,
#     "couplings"  : [
#        Complex(FitParameter("ArS1730_1",  1., -20., 20., 0.01), FitParameter("AiS1730_1",  0.0, -20., 20., 0.01) ),
#        Complex(FitParameter("ArS1730_2",  0., -20., 20., 0.01), FitParameter("AiS1730_2",  0.0, -20., 20., 0.01) )
#                    ]
#  },
#  "fo500" : {
#     "channel"    : 1,
#     "mass"       : Const(0.500),
#     "width"      : Const(0.050),
#     "spin"       : 2,
#     "parity"     : 1,
#     "couplings"  : [
#        Complex(FitParameter("Arfo500_1",  1., -20., 20., 0.01), FitParameter("Aifo500_1",  0.0, -20., 20., 0.01) ),
#        Complex(FitParameter("Arfo500_2",  0., -20., 20., 0.01), FitParameter("Aifo500_2",  0.0, -20., 20., 0.01) ),           
#        Complex(FitParameter("Arfo980_3",  0., -20., 20., 0.01), FitParameter("Aifo980_3",  0.0, -20., 20., 0.01) ),
#        Complex(FitParameter("Arfo980_4",  0., -20., 20., 0.01), FitParameter("Aifo980_4",  0.0, -20., 20., 0.01) )

#		]
#  },
  "fo980" : {
     "channel"    : 1,
     "mass"       : Const(0.980),
     "width"      : Const(0.030),
     "spin"       : 0,
     "parity"     : 1,
     "couplings"  : [
        Complex(FitParameter("Arfo980_1",  1., -20., 20., 0.01), FitParameter("Aifo980_1",  0.0, -20., 20., 0.01) ),
        Complex(FitParameter("Arfo980_2",  0., -20., 20., 0.01), FitParameter("Aifo980_2",  0.0, -20., 20., 0.01) )
       # Complex(FitParameter("Arfo980_3",  0., -20., 20., 0.01), FitParameter("Aifo980_3",  0.0, -20., 20., 0.01) ),
        #Complex(FitParameter("Arfo980_4",  0., -20., 20., 0.01), FitParameter("Aifo980_4",  0.0, -20., 20., 0.01) )
               ]
  },
  "p770" : {
     "channel"    : 1,
     "mass"       : FitParameter("M770",0.770,0.760,0.780,0.0001),
     "width"      : FitParameter("G770",0.07,0.06,0.08,0.0001),
     "spin"       : 1,
     "parity"     : -1,
     "couplings"  : [
        Complex(FitParameter("Arp770_1",  1., -20., 20., 0.01), FitParameter("Aip770_1",  0.0, -20., 20., 0.01) ),
        Complex(FitParameter("Arp770_2",  0., -20., 20., 0.01), FitParameter("Aip770_2",  0.0, -20., 20., 0.01) )          
 #       Complex(FitParameter("Arp770_3",  0., -20., 20., 0.01), FitParameter("Aip770_3",  0.0, -20., 20., 0.01) ),
  #      Complex(FitParameter("Arp770_4",  0., -20., 20., 0.01), FitParameter("Aip770_4",  0.0, -20., 20., 0.01) )
		  ]
  },




#  "Kstar" : {
#    "channel"   : 1,             # Index of bachelor particle, 0="K", "1"="p", "pi"
#    "mass"      : Const(0.892), 
#    "width"     : Const(0.051), 
#    "spin"      : 2, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArKst_1",  1., -20., 20., 0.01), FitParameter("AiKst_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArKst_2",  0., -20., 20., 0.01), FitParameter("AiKst_2",  0.0, -20., 20., 0.01) ),
#        Complex(FitParameter("ArKst_3",  0., -20., 20., 0.01), FitParameter("AiKst_3",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArKst_4",  0., -20., 20., 0.01), FitParameter("AiKst_4",  0.0, -20., 20., 0.01) ),
#                  ]
#  }, 
#  "L1405" : {
#    "channel"   : 2, 
#    "lineshape" : SubThresholdBreitWignerLineShape, 
#    "mass"      : Const(1.405), 
#    "width"     : Const(0.050), 
#    "spin"      : 1, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1405_1",  3., -20., 20., 0.01), FitParameter("AiL1405_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1405_2",  0., -20., 20., 0.01), FitParameter("AiL1405_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1520" : {
#    "channel"   : 2, 
#    "mass"      : FitParameter("M1520", 1.5195, 1.50, 1.55, 0.0001), 
#    "width"     : FitParameter("G1520", 0.0156, 0.01, 0.02, 0.0001), 
#    "spin"      : 3, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(Const(1.), Const(0.) ), 
#        Complex(Const(0.), Const(0.) ),
#                  ]
#  }, 
#  "L1600" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.600), 
#    "width"     : Const(0.150), 
#    "spin"      : 1, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1600_1",  1., -20., 20., 0.01), FitParameter("AiL1600_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1600_2",  0., -20., 20., 0.01), FitParameter("AiL1600_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1670" : {
#    "channel"   : 2, 
#    "mass"      : FitParameter("M1670", 1.670, 1.600, 1.700, 0.0001), 
#    "width"     : FitParameter("G1670", 0.035, 0.020, 0.045, 0.0001), 
#    "spin"      : 1, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1670_1",  1., -20., 20., 0.01), FitParameter("AiL1670_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1670_2",  0., -20., 20., 0.01), FitParameter("AiL1670_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1690" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.690), 
#    "width"     : Const(0.060), 
#    "spin"      : 3, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1690_1",  1., -20., 20., 0.01), FitParameter("AiL1690_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1690_2",  0., -20., 20., 0.01), FitParameter("AiL1690_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1800" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.800), 
#    "width"     : Const(0.300), 
#    "spin"      : 1, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1800_1",  1., -20., 20., 0.01), FitParameter("AiL1800_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1800_2",  0., -20., 20., 0.01), FitParameter("AiL1800_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1810" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.810), 
#    "width"     : Const(0.150), 
#    "spin"      : 1, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1810_1",  1., -20., 20., 0.01), FitParameter("AiL1810_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1810_2",  0., -20., 20., 0.01), FitParameter("AiL1810_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1820" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.820), 
#    "width"     : Const(0.080), 
#    "spin"      : 5, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1820_1",  1., -20., 20., 0.01), FitParameter("AiL1820_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1820_2",  0., -20., 20., 0.01), FitParameter("AiL1820_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1830" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.830), 
#    "width"     : Const(0.095), 
#    "spin"      : 5, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1830_1",  1., -20., 20., 0.01), FitParameter("AiL1830_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1830_2",  0., -20., 20., 0.01), FitParameter("AiL1830_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "L1890" : {
#    "channel"   : 2, 
#    "mass"      : Const(1.890), 
#    "width"     : Const(0.100), 
#    "spin"      : 3, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArL1890_1",  1., -20., 20., 0.01), FitParameter("AiL1890_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArL1890_2",  0., -20., 20., 0.01), FitParameter("AiL1890_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "D1232" : {
#    "channel"   : 0, 
#    "mass"      : Const(1.232), 
#    "width"     : Const(0.100), 
#    "spin"      : 3, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArD1232_1",  5., -20., 20., 0.01), FitParameter("AiD1232_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArD1232_2",  0., -20., 20., 0.01), FitParameter("AiD1232_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "D1600" : {
#    "channel"   : 0, 
#    "mass"      : Const(1.600), 
#    "width"     : Const(0.275), 
#    "spin"      : 3, 
#    "parity"    : 1, 
#    "couplings" : [
#        Complex(FitParameter("ArD1600_1",  0.1, -20., 20., 0.01), FitParameter("AiD1600_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArD1600_2",  0., -20., 20., 0.01), FitParameter("AiD1600_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "D1620" : {
#    "channel"   : 0, 
#    "mass"      : Const(1.620), 
#    "width"     : Const(0.130), 
#    "spin"      : 1, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArD1620_1",  1., -20., 20., 0.01), FitParameter("AiD1620_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArD1620_2",  0., -20., 20., 0.01), FitParameter("AiD1620_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
#  "D1700" : {
#    "channel"   : 0, 
#    "mass"      : Const(1.650), 
#    "width"     : Const(0.230), 
#    "spin"      : 3, 
#    "parity"    : -1, 
#    "couplings" : [
#        Complex(FitParameter("ArD1700_1",  0.1, -20., 20., 0.01), FitParameter("AiD1700_1",  0.0, -20., 20., 0.01) ), 
#        Complex(FitParameter("ArD1700_2",  0. , -20., 20., 0.01), FitParameter("AiD1700_2",  0.0, -20., 20., 0.01) )
#                  ]
#  }, 
}

def listComponentNames(res) : 
  return sorted(res.keys())

def numberOfComponents(res) : 
  return len(listComponentNames(res))

def getHelicityAmplitudes(res, var, sw) : 
  channel = res["channel"]
  if "lineshape" in res : 
    lineShapeFunc = res["lineshape"]
  else : 
    lineShapeFunc = BreitWignerLineShape
  if channel == 1 : 
    lineshape = lineShapeFunc(var["m2pimpip"], res["mass"], res["width"], 
                          mpim, mpip, mp, mlc, dr, db, res["spin"]/2, res["spin"]/2-1)
  if channel == 2 : 
    lineshape = lineShapeFunc(var["m2ppim"], res["mass"], res["width"], 
                          mp, mpim, mpip, mlc, dr, db, OrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)
  if channel == 0 : 
    lineshape = lineShapeFunc(var["m2ppip"], res["mass"], res["width"], 
                          mp, mpip, mpim, mlc, dr, db, OrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)

  ampl = {}

  for pol_l in [-1, 1] : 
    for pol_p in [-1, 1] : 
      # Choose couplings depending on channel and polarisation
      if channel == 1 : # Kpi resonance
        if pol_p == -1 : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l, -2, 0, 0, pol_p, cache = cache)
        else : 
          coupling1 = res["couplings"][2]
          coupling2 = res["couplings"][3]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  2, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache)

      if channel == 0 : 
        if pol_p == -1 : 
          sign = CouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

      if channel == 2 : 
        if pol_p == -1 : 
          sign = CouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

  # Perform rotation of proton spin quantisation axis depending on channel
  rot_angle = SpinRotationAngle( var["p4p"], var["p4k"], var["p4pi"], channel)

  for pol_l in [-1, 1] : 
    a1 = ampl[ (pol_l, -1) ]
    a2 = ampl[ (pol_l,  1) ]
    c = Complex( Cos(rot_angle/2.), Const(0.) )
    s = Complex( Sin(rot_angle/2.), Const(0.) )
    ( ampl[ (pol_l, -1) ], ampl[ (pol_l, 1) ]) = ( c*a1 - s*a2, s*a1 + c*a2 )

  return ampl



phsp = Baryonic3BodyPhaseSpace(mp, mpim, mpip, mlc )

f = TFile.Open("MC_Meerkat_ppipi.root")
effhist = f.Get("kernel")
effhist.SetDirectory(0)
f.Close()

fbck = TFile.Open("BG_Meerkat_ppipi.root")
bckhist = fbck.Get("kernel")
bckhist.SetDirectory(0)
fbck.Close()

#effhist.Smooth()
effshape = RootHistShape(effhist)
bckshape = RootHistShape(bckhist)

switches = Switches(numberOfComponents(resonances)+1)

bck = FitParameter("B",  10., 0., 1000., 0.01)

def model(x) : 

    m2ppim  = phsp.M2ab(x)
    m2ppip = phsp.M2ac(x)
    m2pimpip = phsp.M2bc(x)

    p4p, p4k, p4pi = phsp.FinalStateMomenta(m2ppim, m2pimpip, 0.0, 0.0, 0.0)

    kpi_theta_r, kpi_phi_r, kpi_theta_k, kpi_phi_k = HelicityAngles3Body(p4k, p4pi, p4p)
    pk_theta_r, pk_phi_r, pk_theta_p, pk_phi_p     = HelicityAngles3Body(p4p, p4k, p4pi)
    ppi_theta_r, ppi_phi_r, ppi_theta_p, ppi_phi_p = HelicityAngles3Body(p4p, p4pi, p4k)

    var = {
      "m2ppim" : m2ppim, 
      "m2pimpip" : m2pimpip, 
      "m2ppip" : m2ppip, 
      "p4p" : p4p, 
      "p4k" : p4k, 
      "p4pi" : p4pi, 
      "kpi_theta_r" : kpi_theta_r, 
      "kpi_phi_r"   : kpi_phi_r, 
      "kpi_theta_k" : kpi_theta_k, 
      "kpi_phi_k"   : kpi_phi_k, 
      "pk_theta_r"  : pk_theta_r, 
      "pk_phi_r"    : pk_phi_r, 
      "pk_theta_p"  : pk_theta_p, 
      "pk_phi_p"    : pk_phi_p, 
      "ppi_theta_r" : ppi_theta_r, 
      "ppi_phi_r"   : ppi_phi_r, 
      "ppi_theta_p" : ppi_theta_p, 
      "ppi_phi_p"   : ppi_phi_p, 
    }

    ampls = {}

    for pol_l in [-1, 1] : 
      for pol_p in [-1, 1] : 
        ampls[ (pol_l, pol_p) ] = Complex(Const(0.), Const(0.))

    for i,n in enumerate(listComponentNames(resonances)) : 
      res = resonances[n]
      sw = Complex(switches[i], Const(0.))
      a = getHelicityAmplitudes(res, var, sw)
      for pol_l in [-1, 1] : 
        for pol_p in [-1, 1] : 
          ampls[ (pol_l, pol_p) ] += a[ (pol_l, pol_p) ]
 
    density = Const(0.)
    for pol_l in [-1, 1] : 
      for pol_p in [-1, 1] : 
        density += Density( ampls[ (pol_l, pol_p) ] )

    return (density+bckshape.shape( tf.stack([phsp.M2ab(x), phsp.M2bc(x)], axis = 1))*switches[-1])*effshape.shape( tf.stack([phsp.M2ab(x), phsp.M2bc(x)], axis = 1) )*1e3

data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

data_model = model(data_ph)
norm_model = model(norm_ph)

SetSeed(1)
sess = tf.Session()

init = tf.global_variables_initializer()
sess.run(init)

norm_sample = sess.run( phsp.RectangularGridSample(1000, 1000) )
print "Normalisation sample size = ", len(norm_sample)

#  majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.5
#  print "Maximum = ", majorant
#  data_sample = RunToyMC( sess, data_model, data_ph, phsp, 500000, majorant, chunk = 1000000)

#  f = TFile.Open("toy.root", "RECREATE")
#  FillNTuple("toy", data_sample, ["m2pk", "m2kpi" ])
#  f.Close()

f = TFile.Open("Accepted_PPiPi_tf.root")
nt = f.Get("nt")
data_sample1 = ReadNTuple(nt, [ "m2ppim", "m2pippim" ] ) 
data_sample = sess.run(phsp.Filter(data_sample1) )

nll = UnbinnedNLL( data_model, Integral( norm_model ) )

ReadFitResults(sess, "result_ppipi.txt")

start = timer()
result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = False )
end = timer()
print(end - start) 
print result
WriteFitResults(result, "result_ppipi.txt")

f.Close()

ff = CalculateFitFractions(sess, data_model, data_ph, switches, norm_sample = norm_sample)
WriteFitFractions(ff, listComponentNames(resonances) + ["Bkgr"], "fitfractions.txt")

majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.5
fit_sample = RunToyMC(sess, data_model, data_ph, phsp, 2000000, majorant, chunk = 1000000, switches = switches )
f = TFile.Open("toyresult.root", "RECREATE")
FillNTuple("toy", fit_sample, ["m2pk", "m2kpi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ] )
f.Close()

h1 = TH2F("h1", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
h2 = TH2F("h2", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 

h3 = TH1F("h3", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
h4 = TH1F("h4", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
h5 = TH1F("h5", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h6 = TH1F("h6", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h7 = TH1F("h7", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 
h8 = TH1F("h8", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 

for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

for d in sess.run(phsp.M2ac(data_sample)) : 
    h7.Fill(d)

for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

for f in sess.run(phsp.M2ac(fit_sample)) : 
    h8.Fill(f)

gROOT.ProcessLine(".x lhcbstyle2.C")

c = TCanvas("c","", 900, 600)
c.Divide(3, 2)
gStyle.SetPalette(107)
h4.SetLineColor(2)
h6.SetLineColor(2)
h8.SetLineColor(2)
h1.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h1.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h2.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h2.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h3.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h5.GetXaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h7.GetXaxis().SetTitle("M^{2}(p#pi) [GeV^{2}]")

c.cd(1); h1.Draw("zcol")
c.cd(2); h2.Draw("zcol")
c.cd(4); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
c.cd(5); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
c.cd(6); h7.Draw("e"); h8.Scale(h7.Integral()/h8.Integral()); h8.Draw("h same")
c.Update()
c.Print("Lc2ppipi.pdf")

print(end - start) 
