A collection of useful functions and example scripts to perform the amplitude analysis using TensorFlow

TensorFlow (TF) is an open source library for efficient massively parallel calculations by Google. 
Its primary use is machine learning, but it is also flexible enough to serve as a general-purpose 
tool for efficient calculations, in particular, for maximum-likelihood fits with complex models. 

The main features of TensorFlow essential for maximum likelihood fits are: 

  * Within TF framework, one does not execute operations immediately, but creates a dataflow graph
    which can then be executed. TF performs optimisations of the graph before execution, such as 
    it can identify which operations should be redone when the parameters change, and which results 
    could be taken from cache. 

  * TF provides analytical derivatives for the calculated functions, which speeds up minimisation. 

  * TF runs on many architectures, such as CPU (including mutithreading) and NVidia GPU (incl. 
    multi-GPU configurations). 

  * No knowledge of GPU/parallel programming is required. 

  * Most importantly, TF has a very intuitive Python interface. When programming, one concentrates on 
    the matter of a problem rather than on auxiliary technical stuff. Here's Breit-Wigner:  

    ```python
    def BreitWigner(s, m, width) : 
      return 1./tf.complex(m**2-s, -m*width)
    ```
      
    where s can be a tensor (i.e. an array of data values), m and width can be constants or optimisable 
    variables. Note that these lines already include all the optimisations, such as GPU support, 
    analytical derivatives etc. One can define the set of useful functions and combine them to create 
    fit models in a flexible way. The code is thus very readable and contains only the essence of the 
    physics models. 
    
  * There are other useful features, such as checkpoints (one can dump the state of calculations, to be 
    able to restart the long jobs later). 

TF has its own minimisation functions, but these are optimised for machine learning and not for physics
(e.g. they don't provide errors or likelihood scans). Thus, Minuit needs to be run, but the interface is 
rather straightforward. 

As an illustration, a fully functional Dalitz plot generation/fitting script is provided which uses only 
ROOT and TF as external dependencies: 
  https://gitlab.cern.ch/poluekt/TensorFlowAnalysis/blob/master/demo/DemoDalitzFit.py
There are only ~200 lines of code, where the large part is MINUIT inteface. For the examples of usage of TensorFlowAnalysis library, 
take a look at the files in work/ directory. 

The master branch is compatible with TensorFlow v1.0 or more recent. 